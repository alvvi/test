<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package test
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'test' ); ?></a>
	<header id="masthead" class="header">

		<div class="header__site-branding">
			<div class="hero__container container">
				<div class="row align-items-center">
					<div class="col col-12 col-md-6 text-center text-md-left">
						<a href="<?php bloginfo('url'); ?>" class="header__logo-wrapper">
							<img src="<?php echo ASSETS ?>/img/logo.jpg" alt="Лого" class="header__logo">
						</a>
					</div>
					<div class="col col-12 col-md-6 text-center text-md-right mt-2 mt-md-0">
						<a href="tel:+3806898990" class="header__phone">
							+380 689 89 90
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="header__bg bg">
		
			<nav id="site-navigation" class="header__nav navbar navbar-expand-md navbar-light bg-light pt-3 pb-3 pt-md-2 pb-md-2">
				<div class="container pl-2 pr-2">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<?php 
							wp_nav_menu( array(
								'menu' => 'menu-1',
								'depth' => 2,
								'container' => false,
								'menu_class' => 'navbar-nav mr-auto',
								//Process nav menu using our custom nav walker
								'walker' => new wp_bootstrap_navwalker())
							);
						?>
						<?php dynamic_sidebar( 'sidebar-header' ); ?>
					</div>
				</div>
			</nav>
			
			<?php if(is_front_page()): ?>
			<div class="hero">
				<div class="hero__container container">
				
					<div class="hero__slider">
						<div class="hero__controls btn-group">
							<div class="hero__control hero__control--prev btn btn-primary text-light rounded-left">
								<span class="icon fa fa-play"></span>
							</div>
							<div class="hero__control hero__control--next btn btn-primary text-light rounded-right">
								<span class="icon fa fa-play"></span>
							</div>
						</div>
						<div class="hero__slider-wrapper">
							<div class="hero__slide">
								<div class="row">
									<div class="hero__content-col col col-12 col-lg-4 d-flex flex-column align-items-start">
										<h2 class="hero__heading">
											Туристический сервис рассказал об особенностях отдыха российских мужчин
										</h2>
										<div class="mt-auto hero__date"><span class="icon far fa-clock mr-2"></span> 21 Сентября, 2020</div>
										<span class="hero__badge badge badge-white">Новости</span>
									</div>
									<div class="hero__img-col col col-12 col-lg-8" style="background-image:url(<?php echo ASSETS ?>/img/slides/1.jpg)">
										
									</div>
								</div>
							</div>
							<div class="hero__slide">
								<div class="row">
									<div class="hero__content-col col col-12 col-lg-4 d-flex flex-column align-items-start">
										<h2 class="hero__heading">Впечатления девушки, путешествующей по Сирии автостопом</h2>
										<div class="mt-auto hero__date"><span class="icon far fa-clock mr-2"></span> 3 августа, 2016</div>
										<span class="hero__badge badge badge-white">Статья</span>
									</div>
									<div class="hero__img-col col col-12 col-lg-8" style="background-image:url(<?php echo ASSETS ?>/img/slides/2.jpg)">
										
									</div>
								</div>
							</div>
							<div class="hero__slide">
								<div class="row">
									<div class="hero__content-col col col-12 col-lg-4 d-flex flex-column align-items-start">
										<h2 class="hero__heading">В Мьянме запретят забираться на древние памятники из-за некультурных туристов</h2>
										<div class="mt-auto hero__date"><span class="icon far fa-clock mr-2"></span> 19 декабря, 2016</div>
										<span class="hero__badge badge badge-white">Спорт</span>
									</div>
									<div class="hero__img-col col col-12 col-lg-8" style="background-image:url(<?php echo ASSETS ?>/img/slides/3.jpg)">
										
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>

			</div>
			<?php endif; ?>

		</div>
	</header>

	<div id="content" class="site-content">
