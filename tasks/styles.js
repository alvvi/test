const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

function getPreprocessor(options) {

    if(options.preprocessor === 'sass') {
        return $.sass({
            outputStyle: 'expanded',
            includePaths: ['./node_modules/']
        });
    }

    if(options.preprocessor === 'stylus') {
        return $.stylus();
    }

    return $.if(false, $.plumber());

}
module.exports = (options) => () => {

    const plugins = [
        autoprefixer(),
    ];
    !isDev ? plugins.push(cssnano()) : null 

    return gulp.src(options.src)
        .pipe($.plumber({
            errorHandler: $.notify.onError(err => ({ title: 'Styles', message: err.message }))
        }))
        .pipe($.if(isDev, $.sourcemaps.init()))
        .pipe(getPreprocessor(options))
        .pipe($.postcss(plugins))
        .pipe($.if(isDev, $.sourcemaps.write()))
        .pipe(gulp.dest(options.dest));
};