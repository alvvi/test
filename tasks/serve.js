const browserSync = require('browser-sync').create();

module.exports = (options) => {
    return () => {
        browserSync.init({
            proxy: 'http://projects/logotype',
        });
        
        browserSync
            .watch(options.watchDir)
            .on('change', browserSync.reload);
    };
};