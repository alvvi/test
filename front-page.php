<?php
/**
 * The file for displaying front-page 
 *
 * @package test
 */

get_header();
?>

<?php 
	$news = get_posts(array(
		'post_type'   => 'news',
		'post_status' => 'publish'
	));
?>

<div id="primary" class="content-area">
	<main id="main" class="container site-main">
		<div class="row">
			<div class="site-content__col col col-12 col-lg-4">
				<h2 class="site-content__heading heading text-center text-sm-left">
					О нас
				</h2>
				<div class="about rounded">
					<?php echo do_shortcode( get_post_meta($post->ID, 'О нас', true) ); ?>
				</div>
			</div>
			<div class="site-content__col col col-12 col-lg-8">
				<h2 class="site-content__heading heading text-center text-sm-left">
					Новости
				</h2>
				<div class="news">
					<ul class="news__list">
						<?php 
							foreach( $news as $single_news ) : 
						?>

						<li class="news__item">
							<a href="<?php the_permalink( $single_news ); ?>" class="news__link d-sm-flex text-center text-sm-left">
								<div class="news__preview-col pr-sm-5">

									<?php echo get_the_post_thumbnail( $single_news, 'news_preview', array(
										'class' => 'news__preview rounded',
										'alt'   => 'Превью'
									) ); ?>

								</div>
								<div class="news__content-col mt-3 mt-sm-0">
									<div class="news__date"><?php echo get_the_date( null, $single_news ); ?></div>
									<div class="news__title"><?php echo get_the_title( $single_news ); ?></div>
								</div>
							</a>
						</li>
						
						<?php 
							endforeach; 
						?>
					</ul>
				</div>
			</div>
		</div>
	</main>
</div>

<?php
get_footer();