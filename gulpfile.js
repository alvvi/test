const gulp = require('gulp');
const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const dirs = {
    public: './public',
    src: {
        base: './src',
        components: './src/components',
        pages: './src/pages',
        vendorScripts: './src/js/vendor'
    },
    dist: {
        base: './dist',
        styles: './dist/css',
        js: './dist/js',
        assets: './dist'
    }
}

/**
 * Requires and calls a task with the task name from the path
 * only when the task is actually called
 * @param  {string} taskName
 * @param  {string} path
 * @param  {object} options
 * @return {streamObject}
 */
function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, (callback) => {
        let task = require(path).call(this, options);
        return task(callback);
    });
}

lazyRequireTask('styles', './tasks/styles', {
    src: dirs.src.pages + '/*.{sass,scss}',
    dest: dirs.dist.styles,
    preprocessor: 'sass'
});

lazyRequireTask('clear', './tasks/clear', {
    dest: dirs.dist.base
});

lazyRequireTask('assets', './tasks/copy', {
    src: dirs.public + '/**/*.*',
    dest: dirs.dist.assets
});

lazyRequireTask('assets:components', './tasks/copy', {
    src: dirs.src.pages + '/**/*.{jpg,png}',
    dest: dirs.dist.base
});

lazyRequireTask('assets:vendor-scripts', './tasks/copy', {
    src: dirs.src.vendorScripts + '/**/*.js',
    dest: dirs.dist.js + '/vendor'
});

lazyRequireTask('serve', './tasks/serve', {
    watchDir: [dirs.dist.base + '/**/*.*', '../**/*.php'],
    url: 'http://projects/logotype'
});

lazyRequireTask('webpack', './tasks/webpack', {
    src: dirs.src.pages + '/**/*.js',
    dest: dirs.dist.js,
    config: '../webpack.config.js'
});


/**
 * Combined tasks
 */
gulp.task('build', gulp.series(
    'clear',
    gulp.parallel('styles', 'webpack'),
    gulp.parallel('assets', 'assets:components', 'assets:vendor-scripts')
));

gulp.task('watch', () => {
    gulp.watch(dirs.src.base + '/**/*.{sass,scss}', gulp.series('styles'));
    gulp.watch(dirs.src.base + '/**/*.js', gulp.series('webpack'));

    gulp.watch(dirs.src.components + '/**/*.{jpg,png}', gulp.series('assets:components'));
    gulp.watch(dirs.src.vendorScripts + '/**/*.js', gulp.series('assets:vendor-scripts'));
    gulp.watch(dirs.src.assets + '/**/*.*', gulp.series('assets'));
});

gulp.task('dev', gulp.series(
    'build',
    gulp.parallel('watch', 'serve')
));

gulp.task('prod', gulp.series('build'));
gulp.task('default', gulp.series('dev'));
gulp.task('test', gulp.series('default'));