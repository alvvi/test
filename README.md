# Test Wordpress Theme

## Getting started

Put the theme folder inside wp-content/themes, then inside the theme folder open command line and run first `npm install`, then `npm run prod` to build themes assets or `npm run dev` to start gulp in developing mode

All not "combined" gulp tasks are located in /tasks subfolder, to see *all* tasks run `gulp --tasks`

The source files could be found in /dev as requested in the assignment, but also in /test/src

/test-child folder contains **the child theme**
/colorful-hello-world folder contains the **plugin** mentioned in the assignment