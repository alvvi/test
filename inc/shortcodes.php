<?php
/**
 * Test theme shortcodes
 * 
 * @package test 
 */

function test_hello_world( $atts ){
	return '<div class="test-shortcode">Hello Wordl!</div>';
}
add_shortcode( 'hello_world', 'test_hello_world' );