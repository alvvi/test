<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package test
 */

/**
 * Change month names to more correct Russian forms
 * @return string
 */
function true_russian_date_forms($the_date = '') {
	if ( substr_count($the_date , '---') > 0 ) {
		return str_replace('---', '', $the_date);
	}
	$replacements = array(
		"Январь" => "января",
		"Февраль" => "февраля",
		"Март" => "марта",
		"Апрель" => "апреля",
		"Май" => "мая",
		"Июнь" => "июня",
		"Июль" => "июля",
		"Август" => "августа",
		"Сентябрь" => "сентября",
		"Октябрь" => "октября",
		"Ноябрь" => "ноября",
		"Декабрь" => "декабря"
	);
	return strtr($the_date, $replacements);
}
add_filter('the_time', 'true_russian_date_forms');
add_filter('get_the_time', 'true_russian_date_forms');
add_filter('the_date', 'true_russian_date_forms');
add_filter('get_the_date', 'true_russian_date_forms');
add_filter('the_modified_time', 'true_russian_date_forms');
add_filter('get_the_modified_date', 'true_russian_date_forms');
add_filter('get_post_time', 'true_russian_date_forms');
add_filter('get_comment_date', 'true_russian_date_forms');

/**
 * Markup for Bootstrap search form
 */
function wp_bootstrap_search_form( $form ) {
	$form = '
		<form role="search" method="get" id="searchform" class="searchform form-inline header__form" action="' . home_url( '/' ) . '" >
			<div>
				<label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
				<input class="form-control searchinput" placeholder="Поиск" aria-label="Поиск" type="search" value="' . get_search_query() . '" name="s" id="s" />
			</div>
		</form>';

    return $form;
}

add_filter( 'get_search_form', 'wp_bootstrap_search_form', 100 );