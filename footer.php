<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package test
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="footer">
	<div class="footer__site-branding">
		<div class="footer__container container">
			<div class="row align-items-center">
				<div class="col col-12 col-sm-6 text-center text-sm-left">
					<a href="<?php bloginfo('url'); ?>" class="header__logo-wrapper">
						<img src="<?php echo ASSETS ?>/img/logo-footer.jpg" alt="Лого" class="footer__logo">
					</a>
				</div>
				<div class="col col-12 col-sm-6 text-center text-sm-right mt-4 mt-sm-0">
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				</div>
			</div>
		</div>
	</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
